import vue from 'vue'
import vue_router from 'vue-router'
import Vuex from 'vuex'
vue.use(vue_router);
vue.use(Vuex);
import store from '../data/store.js'
import index_view from '../views/turn_table/index.vue'
import record_view from '../views/turn_table/record.vue'
import { check_lang_key } from '../data/lang.js'
let url_lang = check_lang_key();
const router_link = [
    {
        path: '/',
        redirect: '/' + url_lang,
        name: 'app',
        component: index_view,
    },
    {
      path: '/' + url_lang,
      name: 'app',
      component: index_view
    },
    {
      path: '/record/' + url_lang,
      name: 'record',
      component: record_view
    }
];

const router_config = new vue_router({
    routes: router_link
});

const app = new vue({
    router: router_config
}).$mount('#app');
