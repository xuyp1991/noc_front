import vue from 'vue'
import vue_router from 'vue-router'
import Vuex from 'vuex'
vue.use(vue_router);
vue.use(Vuex);
import index_view from '../components/concat/index.vue'
import block_view from '../components/concat/block_view.vue'
import accounts_view from '../components/concat/accounts_view.vue'
import transaction_view from '../components/concat/transaction_view.vue'
import web_wallet_view from '../components/concat/web_wallet_view.vue'
import contract_account_list from '../components/concat/contract_account_list.vue'
import account_token_list from '../components/concat/account_token_list.vue'
import { check_lang_key } from '../data/lang.js'
let url_lang = check_lang_key();
const router_link = [
    {
        path: '/',
        redirect: '/' + url_lang,
        name: 'app',
        component: index_view,
        children: [
            {
                path: '/' + url_lang ,
                name: 'monitor_view',
                component: resolve => require(['../components/concat/monitor_view.vue'], resolve)
            },
            {
                path: '/' + url_lang + '/web_wallet_view',
                name: 'web_wallet_view',
                component: web_wallet_view
            },
            {
                path: '/' + url_lang + '/block_view',
                name: 'block_view',
                component: block_view
            },
            {
                path: '/' + url_lang + '/block_detail_view/:block_num',
                name: 'block_detail_view',
                component: resolve => require(['../components/concat/block_detail_view.vue'], resolve)
            },
            {
                path: '/' + url_lang + '/accounts_view',
                name: 'accounts_view',
                component: accounts_view
            },
            {
                path: '/' + url_lang + '/account_detail_view/:account_name',
                name: 'account_detail_view',
                component: resolve => require(['../components/concat/account_detail_view.vue'], resolve)
            },
            {
                path: '/' + url_lang + '/transaction_view',
                name: 'transaction_view',
                component: transaction_view
            },
            {
                path: '/' + url_lang + '/transaction_detail_view/:trx_id',
                name: 'transaction_detail_view',
                component: resolve => require(['../components/concat/transaction_detail_view.vue'], resolve)
            },
            {
                path: '/' + url_lang + '/contract_account_list',
                name: 'contract_account_list',
                component: contract_account_list
            },
            {
                path: '/' + url_lang + '/contract_account_detail/:account_name',
                name: 'contract_account_detail',
                component: resolve => require(['../components/concat/contract_account_detail.vue'], resolve)
            },
            {
                path: '/' + url_lang + '/account_token_list',
                name: 'account_token_list',
                component: account_token_list
            },
            {
                path: '/' + url_lang + '/account_token_detail/:symbol',
                name: 'account_token_detail',
                component: resolve => require(['../components/concat/account_token_detail.vue'], resolve)
            },
            {
                path: '/' + url_lang + '/eth_eos_mapping',
                name: 'eth_eos_mapping',
                component: resolve => require(['../components/concat/eth_eos_mapping_view.vue'], resolve)
            },
            {
                path: '/' + url_lang + '/eos_public_key_view/:eos_public_key',
                name: 'eos_public_key_view',
                component: resolve => require(['../components/concat/eos_public_key_view.vue'], resolve)
            },
            {
                path: '/' + url_lang + '/search_res_view/:account_name/:block_num/:trx_id/:eos_public_key',
                name: 'search_res_view',
                component: resolve => require(['../components/concat/search_res_view.vue'], resolve)
            }
        ]
    }
];

const router_config = new vue_router({
    routes: router_link
});

const app = new vue({
    router: router_config
}).$mount('#app');

