import vue from 'vue'
import vue_router from 'vue-router'
import Vuex from 'vuex'
vue.use(vue_router);
vue.use(Vuex);

import index_view from '../views/FAQ/layout.vue'

import { check_lang_key } from '../data/lang.js'
let url_lang = check_lang_key();
const router_link = [
    {
        path: '/',
        redirect: '/' + url_lang,
        name: 'app',
        component: index_view,
        children: [
            {
                path: '/' + url_lang + '/:h1_id/:h2_id/' ,
                name: 'forceio',
                component: resolve => require(['../views/FAQ/qa.vue'], resolve)
            },
            {
                path: '/' + url_lang,
                name: 'forceio',
                redirect: '/' + url_lang + '/1/1.1/',
                component: resolve => require(['../views/FAQ/qa.vue'], resolve)
            }
        ]
    }
];

const router_config = new vue_router({
    routes: router_link
});

const app = new vue({
    router: router_config
}).$mount('#app');

