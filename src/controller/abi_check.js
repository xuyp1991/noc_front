import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);
import abi_check from '../components/abi_check/abi_check.vue'

var app = new Vue({
  el: '#app',
  components:{
    abi_check
  }
});