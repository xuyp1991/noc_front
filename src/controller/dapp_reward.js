import vue from 'vue'
import vue_router from 'vue-router'
import Vuex from 'vuex'
vue.use(vue_router);
vue.use(Vuex);
import dapp_reward from '../components/dapp_reward/index.vue'

const router_link = [
    {
        path: '/',
        name: 'app',
        component: dapp_reward
    }
]

const router_config = new vue_router({
    routes: router_link
});

const app = new vue({
    router: router_config
}).$mount('#app');