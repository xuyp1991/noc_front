import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);
import force_bch from '../components/force_bch/force_bch.vue'

var app = new Vue({
  el: '#app',
  components:{
    force_bch
  }
});