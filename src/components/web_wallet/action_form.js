import { lang } from '../../data/lang.js'

const main_choice = [
  {
        type: 'item_select',
        title: lang.data.added_vote,
        placeholder: lang.data.added_vote,
        list: [
          {value: '1', text: lang.data['活期']},
          {value: '2', text: lang.data['定期']},
        ],
        param: true,
        param_index: 1,
        name: 'vote_type',
        value: '1',
        exe: 'EOSC'
    },
    {
        type: 'item_select',
        title: lang.data.added_vote,
        placeholder: lang.data.added_vote,
        list: [
          {value: '1', text: lang.data['投票']},
          {value: '2', text: lang.data['赎回']},
          {value: '3', text: lang.data['转投']},
        ],
        param: true,
        param_index: 1,
        name: 'vote_action',
        value: '1',
        exe: 'EOSC'
    }
]
export const create_revote_form = (frombp, bp_list, staked) => {
    let show_list = [
        {
            type: 'txt',
            title: lang.data.from_bp,
            name: 'frombp',
            param: true,
            param_index: 0,
            value: frombp
        },
        {
            type: 'txt',
            title: lang.data.my_vote,
            param: false,
            name: 'staked',
            value: parseFloat(staked) + ' EOSC'
        },
        // {
        //     type: 'txt',
        //     title: lang.data.fee,
        //     param: true,
        //     name: 'fee',
        //     value: '0.0100 EOSC'
        // },
        
        // {
        //     type: 'txt',
        //     title: lang.data.available,
        //     param: true,
        //     name: 'available',
        //     value: available
        // },
        {
            type: 'select',
            title: lang.data.to_bp,
            placeholder: lang.data.to_bp,
            param: true,
            param_index: 1,
            list: bp_list,
            name: 'tobp',
            value: '',
            exe: 'EOSC'
        },
        {
            type: 'input',
            title: lang.data.transfer_ammount,
            placeholder: lang.data.transfer_ammount,
            param: true,
            param_index: 1,
            name: 'restake',
            value: '',
            exe: 'EOSC'
        },
        {
            type: 'txt',
            title: lang.data.fee,
            param: true,
            name: 'fee',
            value: '0.0100 EOSC'
        },
    ]

    show_list.splice(0, 0, ...main_choice);

    show_list.get_params = (show_list = show_list) => {
        let args = {};
        show_list.filter(item => {
            if(item.param){
                args[item.name] = item.value;
            }
        });
        args.restake = parseInt(args.restake);
        args.fee = parseInt(args.fee)||0;
        if(!args.restake){
            return {
                is_error: true,
                msg: lang.data.vote_more_than_zero || '转投金额不能为0'
            }
        }
        if(!args.tobp){
            return {
                is_error: true,
                msg: lang.data.vote_more_than_zero || '未选择转投节点'
            }
        }
        let total = args.staked + args.change;
        return [args.frombp, args.tobp, args.restake + ' EOS'];
    }

    let proxy = new Proxy(show_list, {
        set (target, key, receiver) {
           show_list.find(item => {
            if(item.name == key){
                item.value = receiver;
            }
           }) 
        }
    })
    return {
        proxy,
        show_list
    }
}

export const create_vote_form = (bpname, staked, available, change) => {
    let show_list = [
        {
            type: 'txt',
            title: lang.data.bpname,
            name: 'bpname',
            param: true,
            param_index: 0,
            value: bpname
        },
        {
            type: 'txt',
            title: lang.data.my_vote,
            param: true,
            name: 'staked',
            value: parseFloat(staked) + ' EOSC',
            exe: 'EOSC'
        },
        {
            type: 'txt',
            title: lang.data.fee,
            param: true,
            name: 'fee',
            value: '0.0500 EOSC'
        },
        {
            type: 'txt',
            title: lang.data.available,
            param: true,
            name: 'available',
            value: parseFloat(available) + ' EOSC',
            exe: 'EOSC'
        },
        {
            type: 'input',
            title: lang.data.added_vote,
            placeholder: lang.data.added_vote,
            param: true,
            param_index: 1,
            name: 'change',
            value: '',
            exe: 'EOSC'
        },
        // 
    ]

    show_list.splice(0, 0, ...main_choice);

    show_list.get_params = (show_list = show_list) => {
        let args = {};
        show_list.filter(item => {
            if(item.param){
                args[item.name] = item.value;
            }
        });
        args.change = parseInt(args.change);
        args.staked = parseInt(args.staked)||0;
        args.available = parseInt(args.available)||0;
        args.fee = parseInt(args.fee)||0;
        if(!args.change){
            return {
                is_error: true,
                msg: lang.data.vote_more_than_zero || '投注金额不能为0'
            }
        }
        if(args.change + args.fee > args.available){
            return {
                is_error: true,
                msg: lang.data.not_enough_eos || '可用余额不足'
            };
        }
        let total = args.staked + args.change;
        return [total, args.bpname];
    }

    let proxy = new Proxy(show_list, {
        set (target, key, receiver) {
           show_list.find(item => {
            if(item.name == key){
                item.value = receiver;
            }
           }) 
        }
    });
    return {
        proxy,
        show_list
    }
}

export const create_minus_fixed_form = (voter, bpname, fixed_list, bp_list) => {
    // voter, bpname, type, vote_num, stake_typ = 1
    let show_list = [
        {
            type: 'txt',
            title: lang.data.bpname,
            name: 'voter',
            param: true,
            param_index: 0,
            hide: true,
            value: voter
        },
        {
            type: 'txt',
            title: lang.data.bpname,
            name: 'bpname',
            param: true,
            param_index: 0,
            value: bpname
        },
        {
            type: 'txt',
            title: lang.data.fee,
            param: true,
            name: 'fee',
            value: '0.0500 EOSC'
        },
        {
            type: 'select',
            title:lang.data['选择到期投票记录'] || '选择到期投票记录',
            placeholder: lang.data.added_vote,
            list: fixed_list,
            param: true,
            param_index: 1,
            name: 'fixed_key',
            value: '',
            exe: 'EOSC'
        },
        // 
    ]

    show_list.splice(0, 0, ...main_choice);

    show_list.find(item => item.name == 'vote_type').value = 2;
    show_list.find(item => item.name == 'vote_action').value = 2;

    show_list.get_params = (show_list = show_list) => {
        let args = {};
        show_list.filter(item => {
            if(item.param){
                args[item.name] = item.value;
            }
        });
        args.vote_num = parseInt(args.vote_num);
        args.available = parseInt(args.available) || 0;
        args.unstaking = parseInt(args.unstaking) || 0;
        args.fix_vote = parseInt(args.fix_vote) || 0;
        args.fee = parseInt(args.fee) || 0;
        // ({voter, fixed_key, bpname,
        if(args.fixed_key == ''){
          return {
            is_error: true,
            msg: 'not select fixed vote record'
          }
        }

        if(!args.bpname){
          return {
            is_error: true,
            msg: 'not choose a bp for transfer'
          }
        }


        return [args.voter, args.fixed_key, args.bpname]
    }

    let proxy = new Proxy(show_list, {
        set (target, key, receiver) {
           show_list.find(item => {
            if(item.name == key){
                item.value = receiver;
            }
           }) 
        }
    });
    return {
        proxy,
        show_list
    }
}

// create_fixed_revote_form

export const create_fixed_revote_form = (voter, pre_bp_name, fixed_list, bp_list) => {
    // voter, bpname, type, vote_num, stake_typ = 1
    let show_list = [
        {
            type: 'txt',
            title: lang.data.bpname,
            name: 'voter',
            param: true,
            param_index: 0,
            hide: true,
            value: voter
        },
        {
            type: 'txt',
            title: lang.data.bpname,
            name: 'pre_bp_name',
            param: true,
            param_index: 0,
            value: pre_bp_name
        },
        {
            type: 'txt',
            title: lang.data.fee,
            param: true,
            name: 'fee',
            value: '0.0500 EOSC'
        },
        {
            type: 'select',
            title:lang.data['选择投票记录'] || '选择投票记录',
            placeholder: lang.data.added_vote,
            list: fixed_list,
            param: true,
            param_index: 1,
            name: 'fixed_key',
            value: '',
            exe: 'EOSC'
        },
        {
            type: 'select',
            title: lang.data.to_bp || '选择转投BP',
            placeholder: lang.data.added_vote,
            list: bp_list,
            param: true,
            param_index: 1,
            name: 'bpname',
            value: '',
            exe: 'EOSCdd'
        },
        // 
    ]

    show_list.splice(0, 0, ...main_choice);

    show_list.find(item => item.name == 'vote_type').value = 2;
    show_list.find(item => item.name == 'vote_action').value = 3;

    show_list.get_params = (show_list = show_list) => {
        let args = {};
        show_list.filter(item => {
            if(item.param){
                args[item.name] = item.value;
            }
        });
        args.vote_num = parseInt(args.vote_num);
        args.available = parseInt(args.available) || 0;
        args.unstaking = parseInt(args.unstaking) || 0;
        args.fix_vote = parseInt(args.fix_vote) || 0;
        args.fee = parseInt(args.fee) || 0;

        if(!args.fixed_key && args.fixed_key != 0){
          return {
            is_error: true,
            msg: 'not select fixed vote record'
          }
        }
        if(!args.bpname){
          return {
            is_error: true,
            msg: 'not choose a bp for transfer'
          }
        }


        return [args.voter, args.pre_bp_name, args.fixed_key, args.bpname]
    }

    let proxy = new Proxy(show_list, {
        set (target, key, receiver) {
           show_list.find(item => {
            if(item.name == key){
                item.value = receiver;
            }
           }) 
        }
    });
    return {
        proxy,
        show_list
    }
}

export const create_fixed_vote_form = (voter, bpname, available, unstaking, fix_vote) => {
    // voter, bpname, type, vote_num, stake_typ = 1
    let show_list = [
        {
            type: 'txt',
            title: lang.data.bpname,
            name: 'voter',
            param: true,
            param_index: 0,
            hide: true,
            value: voter
        },
        {
            type: 'txt',
            title: lang.data.bpname,
            name: 'bpname',
            param: true,
            param_index: 0,
            value: bpname
        },
        {
            type: 'txt',
            title: lang.data.fee,
            param: true,
            name: 'fee',
            value: '0.0500 EOSC'
        },
        {
            type: 'item_select',
            title: lang.data['投票来源'],
            placeholder: lang.data.added_vote,
            list: [
              {value: '1', text: lang.data['余额中']},
              {value: '2', text: lang.data['赎回中']},
              {value: '3', text: lang.data['活期投票中']},
            ],
            param: true,
            param_index: 1,
            name: 'stake_typ',
            value: '1',
            exe: 'EOSC'
        },
        {
            type: 'txt',
            title: lang.data.available,
            param: true,
            name: 'available',
            value: parseFloat(available) + ' EOSC'
        },
        {
            type: 'txt',
            title: lang.data['正在赎回中'] || '正在赎回中',
            param: true,
            name: 'unstaking',
            value: parseFloat(unstaking) + ' EOSC'
        },
        {
            type: 'txt',
            title: lang.data['活期投票'] || '活期投票',
            param: true,
            name: 'fix_vote',
            value: parseFloat(fix_vote) + ' EOSC'
        },
        {
            type: 'item_select',
            title: lang.data.added_vote,
            placeholder: lang.data.added_vote,
            list: [
              {value: 'fvote.d', text: '720 ' + lang.data['天']},
              {value: 'fvote.c', text: '360 ' + lang.data['天']},
              {value: 'fvote.b', text: '180 ' + lang.data['天']},
              {value: 'fvote.a', text: '90 ' + lang.data['天']},
            ],
            param: true,
            param_index: 1,
            name: 'type',
            value: 'fvote.d',
            exe: 'EOSC'
        },
        {
            type: 'input',
            title: lang.data.added_vote,
            placeholder: lang.data.added_vote,
            param: true,
            param_index: 1,
            name: 'vote_num',
            value: '',
            exe: 'EOSC'
        },
        // 
    ]

    show_list.splice(0, 0, ...main_choice);

    show_list.find(item => item.name == 'vote_type').value = 2;
    show_list.find(item => item.name == 'vote_action').value = 1;

    show_list.get_params = (show_list = show_list) => {
        let args = {};
        show_list.filter(item => {
            if(item.param){
                args[item.name] = item.value;
            }
        });
        args.vote_num = parseInt(args.vote_num);
        args.available = parseInt(args.available) || 0;
        args.unstaking = parseInt(args.unstaking) || 0;
        args.fix_vote = parseInt(args.fix_vote) || 0;
        args.fee = parseInt(args.fee) || 0;

        if(!args.vote_num){
            return {
                is_error: true,
                msg: lang.data.vote_more_than_zero || '投注金额不能为0'
            }
        }

        if(args.vote_num + args.fee > args.available && args.stake_typ == '1'){
            return {
                is_error: true,
                msg: 'vote_num + fee > available, it should be vote_num + fee <= available'
            };
        }

        if(args.vote_num > args.unstaking && args.stake_typ == '2'){
            return {
                is_error: true,
                msg: 'vote_num > unstaking, it should be vote_num <= unstaking'
            };
        }

        if(args.vote_num > args.fix_vote && args.stake_typ == '3'){
          return {
                is_error: true,
                msg: 'vote_num > staked, it should be vote_num <= staked'
            };
        }
        if(args.vote_num + args.unstaking < 2 && args.stake_typ == '3'){
          return {
            is_error: true,
            msg: 'vote_num + unstaking < 2, it should be vote_num + unstaking > 2'
          }
        }

        return [args.voter, args.bpname, args.type, args.vote_num, parseInt(args.stake_typ), args.fix_vote, args.unstaking]
    }

    let proxy = new Proxy(show_list, {
        set (target, key, receiver) {
           show_list.find(item => {
            if(item.name == key){
                item.value = receiver;
            }
           }) 
        }
    });
    return {
        proxy,
        show_list
    }
}


export const create_claim_form = (bpname, ammout, voter) => {
    let show_list = [
        {
            type: 'txt',
            title: lang.data.bpname,
            name: 'bpname',
            param: true,
            param_index: 0,
            value: bpname
        },
        {
            type: 'txt',
            title: lang.data.voter,
            param: false,
            name: 'voter',
            value: voter
        },
        {
            type: 'txt',
            title: lang.data.fee,
            param: true,
            name: 'fee',
            value: '0.0300 EOSC'
        },
        {
            type: 'txt',
            title: lang.data.your_reward,
            param: true,
            name: 'ammout',
            value: ammout
        }
    ]

    show_list.get_params = () => {
        let args = {};
        show_list.filter(item => {
            if(item.param){
                args[item.name] = item.value;
            }
        });
        args.fee = parseInt(args.fee)||0;
        if(!args.bpname){
            return {
                is_error: true,
                msg: lang.data.choose_a_bp || '请选择节点'
            }
        }
        if(args.fee > args.available){
            return {
                is_error: true,
                msg: lang.data.not_enough_eos || '可用余额不足'
            };
        }
        return [args.bpname];
    }

    let proxy = new Proxy(show_list, {
        set (target, key, receiver) {
           show_list.find(item => {
            if(item.name == key){
                item.value = receiver;
            }
           }) 
        }
    })
    return {
        proxy,
        show_list
    }
}

export const create_minus_vote_form = (bpname, staked, available, change) => {
    let show_list = [
        {
            type: 'txt',
            title: lang.data.bpname,
            name: 'bpname',
            param: true,
            param_index: 0,
            value: bpname
        },
        {
            type: 'txt',
            title: lang.data.fee,
            param: true,
            name: 'fee',
            value: '0.0500 EOSC'
        },
        {
            type: 'txt',
            title: lang.data.available,
            param: true,
            name: 'available',
            value: parseFloat(available) + ' EOSC'
        },
        {
            type: 'txt',
            title: lang.data.number_you_redemption,
            param: true,
            name: 'staked',
            value: parseFloat(staked) + ' EOSC'
        },
        {
            type: 'input',
            title: lang.data.redemption_num,
            param: true,
            param_index: 1,
            name: 'change',
            value: '',
            exe: 'EOSC',
            info_txt: lang.data.minus_vote_info
        }
    ]

    show_list.splice(0, 0, ...main_choice);
    show_list.get_params = (show_list = show_list) => {
        let args = {};
        show_list.filter(item => {
            if(item.param){
                args[item.name] = item.value;
            }
        });
        args.change = parseInt(args.change);
        args.staked = parseInt(args.staked) || 0;
        args.available = parseInt(args.available) || 0;
        args.fee = parseInt(args.fee) || 0;
        if(!args.change){
            return {
                is_error: true,
                msg: lang.data.more_than_zero_of_minus || '赎回金额必须大于0'
            }
        }
        if(args.fee > args.available){
            return {
                is_error: true,
                msg: lang.data.not_enough_eos || '可用余额不足'
            };
        }
        if(args.change > args.staked){
            return {
                is_error: true,
                msg: lang.data.no_more_than_vote || '赎回金额不能大于已有投票数额'
            };
        }
        let total = args.staked - args.change;
        return [total, args.bpname];
    }

    let proxy = new Proxy(show_list, {
        set (target, key, receiver) {
           show_list.find(item => {
            if(item.name == key){
                item.value = receiver;
            }
           }) 
        }
    })
    return {
        proxy,
        show_list
    }
}


export const create_transfer_form = (available) => {
    let show_list = [
        {
            type: 'txt',
            title: lang.data.fee,
            param: true,
            name: 'fee',
            value: '0.0100 EOSC'
        },
        {
            type: 'txt',
            title: lang.data.available,
            param: true,
            name: 'available',
            value: parseFloat(available) + ' EOSC'
        },
        {
            type: 'input',
            title: lang.data.transfer_to || '转账对象',
            param: true,
            param_index: 1,
            name: 'transfer_to',
            value: '',
            exe: ''
        },
        {
            type: 'input',
            title: lang.data.transfer_number || '转账数额',
            param: true,
            param_index: 1,
            placeholder: 'EOSC',
            name: 'transfer_number',
            value: '',
            exe: ''
        },
        {
            type: 'max_input',
            title: lang.data.transfer_comment|| '转账数额',
            param: true,
            param_index: 1,
            placeholder: lang.data.max_words_256,
            name: 'transfer_comment',
            value: '',
            exe: ''
        }
    ]

    show_list.get_params = (is_dict = false) => {
        let args = {};
        show_list.filter(item => {
            if(item.param){
                args[item.name] = item.value;
            }
        });
        args.transfer_number = parseFloat(args.transfer_number);
        args.staked = parseInt(args.staked)||0;
        args.available = parseInt(args.available)||0;
        args.fee = parseInt(args.fee)||0;
        if(!args.transfer_to && args.transfer_to != 0){
            return {
                is_error: true,
                msg: lang.data.has_no_transfer_object || '转账对象未填写'
            }
        }
        if(!args.transfer_number){
            return {
                is_error: true,
                msg: lang.data.transfer_num_error || '转账数额不能为0, 并且只能为数字'
            }
        }
        if(args.transfer_comment.length > 256){
            return {
                is_error: true,
                msg: lang.data.transfer_comment_num_err || '最大长度不能大于256'
            }
        }
        let result = null;
        if(!is_dict){
            result = [args.transfer_to, args.transfer_number, args.transfer_comment];
        }else{
            result = args;
        }
        return result;
    }

    let proxy = new Proxy(show_list, {
        set (target, key, receiver) {
           show_list.find(item => {
            if(item.name == key){
                item.value = receiver;
            }
           }) 
        }
    })
    return {
        proxy,
        show_list
    }
}

export const create_unfreeze_form = (bpname, voter, unstaking_num) => {
    let show_list = [
        {
            type: 'txt',
            title: lang.data.bpname,
            name: 'bpname',
            param: true,
            param_index: 0,
            value: bpname
        },
        {
            type: 'txt',
            title: lang.data.fee,
            param: true,
            name: 'fee',
            value: '0.01 EOSC'
        },
        {
            type: 'txt',
            title: lang.data.voter,
            param: false,
            name: 'voter',
            value: voter
        },
        {
            type: 'txt',
            title: lang.data.unstaking_num || '解冻金额',
            param: true,
            param_index: 1,
            name: 'unstaking_num',
            value: unstaking_num,
            exe: ''
        }
    ]

    show_list.get_params = () => {
        let args = {};
        show_list.filter(item => {
            if(item.param){
                args[item.name] = item.value;
            }
        });
        
        return [args.bpname];
    }

    let proxy = new Proxy(show_list, {
        set (target, key, receiver) {
           show_list.find(item => {
            if(item.name == key){
                item.value = receiver;
            }
           }) 
        }
    })
    return {
        proxy,
        show_list
    }
}

// transfer_account

export const create_transfer_account = (account_name, active_public_key, owner_public_key) => {
    let show_list = [
        {
            type: 'txt',
            title: lang.data.account_name,
            name: 'account_name',
            // param: true,
            param_index: 0,
            value: account_name
        },
        {
            type: 'txt',
            type: 'max_input',
            title: lang.data.active_public_key,
            name: 'active_public_key',
            param: true,
            param_index: 0,
            value: active_public_key
        },
        {
            type: 'txt',
            type: 'max_input',
            title: lang.data.owner_public_key,
            name: 'owner_public_key',
            param: true,
            param_index: 0,
            value: owner_public_key
        },
        {
            type: 'txt',
            title: lang.data.fee,
            // param: true,
            name: 'fee',
            value: '0.2 EOSC'
        },
        // {
        //     type: 'txt',
        //     title: lang.data.voter,
        //     param: false,
        //     name: 'voter',
        //     value: voter
        // },
        // {
        //     type: 'txt',
        //     title: lang.data.unstaking_num || '解冻金额',
        //     param: true,
        //     param_index: 1,
        //     name: 'unstaking_num',
        //     value: unstaking_num,
        //     exe: ''
        // }
    ]

    show_list.get_params = () => {
        let args = {};
        show_list.filter(item => {
            if(item.param){
                args[item.name] = item.value;
            }
        });
        
        return [args.active_public_key, args.owner_public_key];
    }

    let proxy = new Proxy(show_list, {
        set (target, key, receiver) {
           show_list.find(item => {
            if(item.name == key){
                item.value = receiver;
            }
           }) 
        }
    })
    return {
        proxy,
        show_list
    }
}