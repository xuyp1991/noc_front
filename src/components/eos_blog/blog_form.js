import { lang } from '../../data/lang.js'
export const create_blog_from = (author, title, illustration, brief, content, is_published) => {
    let show_list = [
        {
            type: 'input',
            title: lang.data.id || 'ID',
            placeholder: lang.data.id || 'ID',
            param: true,
            param_index: 1,
            name: 'id',
            value: '',
            exe: '',
            hide: true
        },
        {
            type: 'input',
            title: lang.data.author || '作者',
            placeholder: lang.data.author || '作者',
            param: true,
            param_index: 1,
            name: 'author',
            value: '',
            exe: ''
        },
        {
            type: 'input',
            title: lang.data.title || '标题',
            placeholder: lang.data.title || '标题',
            param: true,
            param_index: 1,
            name: 'title',
            value: '',
            exe: ''
        },
        {
            type: 'input',
            title: lang.data.illustration || '插图',
            placeholder: lang.data.illustration || '插图',
            param: true,
            param_index: 1,
            name: 'illustration',
            value: '',
            exe: ''
        },
        {
            type: 'input',
            title: lang.data.brief || '简介',
            placeholder: lang.data.brief || '列表显示时使用的缩略',
            param: true,
            param_index: 1,
            name: 'brief',
            value: '',
            exe: ''
        },
        {
            type: 'max_input',
            title: lang.data.content || '内容',
            placeholder: lang.data.content || 'markdown格式',
            param: true,
            param_index: 1,
            name: 'content',
            value: '',
            exe: ''
        },
        {
            type: 'input',
            title: lang.data.password || '验证密码',
            placeholder: lang.data.content || '密码',
            param: true,
            param_index: 1,
            name: 'password',
            value: '',
            exe: ''
        }
    ]

    show_list.get_params = ( is_dict = false ) => {
        let args = {};
        show_list.filter(item => {
            if(item.param){
                args[item.name] = item.value;
            }
        });
        
        if(!args.author){
            return {
                is_error: true,
                msg: 'no author'
            }
        }
        if(!args.title){
            return {
                is_error: true,
                msg: 'no title'
            }
        }
        if(!args.illustration){
            return {
                is_error: true,
                msg: 'no illustration'
            }
        }
        // brief
        if(!args.brief){
            return {
                is_error: true,
                msg: 'no brief'
            }
        }
        if(!args.content){
            return {
                is_error: true,
                msg: 'no content'
            }
        }
        if(!args.password){
            return {
                is_error: true,
                msg: 'no password'
            }
        }
        let result = null;
        if(!is_dict){
            result = [args.author, args.title, args.illustration, args.brief, args.content];
        }else{
            result = args;
        }
        return result;
    }

    let proxy = new Proxy(show_list, {
        set (target, key, receiver) {
           show_list.find(item => {
            if(item.name == key){
                item.value = receiver;
            }
           });
           return true;
        }
    })
    return {
        proxy,
        show_list
    }
}