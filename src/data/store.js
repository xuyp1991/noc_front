import Vuex from 'vuex'
import Vue from 'vue'
import ScatterJS from 'scatterjs-core'
import ScatterEOS from 'scatterjs-plugin-eosjs'
import Eos from 'eosforcejs'
// import Eos from 'eosforce'
import monitor_brief from './monitor_brief_store.js'
import global_config from './global_config_store.js'

Vue.use(Vuex);
ScatterJS.plugins( new ScatterEOS() );
const store = new Vuex.Store({
  modules: {
    global_config,
    monitor_brief
  }
});

store.dispatch('check_is_mobile');
window.addEventListener('resize', () => {
    store.dispatch('check_is_mobile');
});

// scatter load 
document.addEventListener('scatterLoaded', scatterExtension => {
    store.dispatch('check_scatter');
});

const init_scatter = async () => {
  let connected = await ScatterJS.scatter.connect("My-App");
  if(!connected) return false;
  window.connected = connected;

  const scatter = ScatterJS.scatter;
  window.scatter = scatter;
  
  store.dispatch('check_scatter');
}

init_scatter();

export default store;