import Eos from 'eosforcejs'

const scatter_res = {
    'eos': null,
    'account_name': null,
    'permission': null,
    'has_reject': false,
    'is_running': false
}

const string_to_name = ( _str ) => {
    let int_num = parseInt(_str);
    if (!int_num || ('' + int_num ).length != (_str + '').length) {
        return _str;
    }
    let name = new Array(64);
    for(let _i of name.keys()) { name[_i] = 0 };
    let i = 0;
    for ( i ; _str[i] && i < 12; ++i) {
       let symbol = ten_to_bit((char_to_symbol(_str[i]) & 0x1f )),
       pos = 64 - 5 * (i + 1) - 1;
       let moved_bites = move_bite(symbol, pos);
       or_bits(name, moved_bites);
   }
   if (i == 12){
      or_bits(name, ten_to_bit((char_to_symbol(_str[12]) & 0x1f )));
   }
   return bits_to_ten(name).toString();
}

const wait_time = ( _time = 2000) => {
    return new Promise((rs, rj) => {
        let cl = setTimeout(() => {
            clearTimeout(cl);
            rs();
        }, _time)
    });
}

export const get_scatter_identity = async (network, exchange_account = false) => {
    let identity = await scatter.getIdentity({accounts:[network]}).catch(err => err);
    if(identity.isError){
        return {
            is_error: true,
            msg: identity
        }
    }
    const account = identity.accounts.find(function(acc){
         return acc.blockchain === 'eos';
    });
    let options = {
     authorization: account.name+'@'+account.authority,
     broadcast: true,
     sign: true
    }    
    let eos = scatter.eos(network, Eos,  options, "https");
    // scatter_res.is_running = false;
    return {
        is_error: false,
        data: {eos, account_name: account.name, permission: account.authority}
    };
}

export const quit_scatter = async () => {
    await scatter.forgetIdentity(); 
}

export const get_abi = async (httpEndpoint, account_name) => {
    let res = await Eos({ httpEndpoint }).getAbi({account_name});
    return res;
}
