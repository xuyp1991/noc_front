import axios from 'axios'
import Eos from 'eosjs'

export const option = {
  httpEndpoint: 'http://47.75.200.188:8001', // default
  app_host: 'http://47.52.6.238:3002'
}

const log = (...args) => {
    console.log(...args);
}

export const get_block_list = (page = 1, step = 10) => {
    return new Promise((resolve, reject) => {
        axios.post(option.app_host + '/web/get_block_list', {
            page,
            step
        })
        .then(res => {
            resolve(res.data);
        })
        .catch(err => {
            resolve(null);
        });
    });
}

export const get_block = (block_num = 0, block_num_or_id = 0) => {
    return new Promise((resolve, reject) => {
        axios.post(option.app_host + '/web/get_block', {
            block_num_or_id: block_num_or_id
        })
        .then( res => {
            resolve(res.data);
        })
        .catch(err => {
            resolve(null);
        })
    });
}

export const test_get_info = (block_num = 0, block_num_or_id = 0) => {
    return new Promise((resolve, reject) => {
        axios.get(option.app_host + '/web/get_info', {
            block_num_or_id: block_num_or_id
        })
        .then( res => {
            resolve(res.data);
        })
        .catch(err => {
            resolve(null);
        })
    });
}

export const get_supply = async () => {
  let data = await axios.post(option.httpEndpoint + '/v1/chain/get_table_rows', {
            code: 'bacc.token',
            scope: 'stat',
            table: 'stat',
            json: true,
            limit: 1
        });

  console.log('suppy is', data);
}

get_supply();

export const get_accounts = (page = 1, step = 10) => {
    return new Promise((resolve, reject) => {
        axios.post(option.app_host + '/web/get_accounts', {
            page,
            step
        })
        .then((res) => {
            resolve(res.data);
        })
        .catch(err => {
            resolve(null);
        })
    });
}

export const get_account_info = (account_name) => {
    return new Promise((resolve, reject) => {
        axios.post(option.app_host + '/web/get_account_info', {
            account_name
        })
        .then((res) => {
            resolve(res.data.data);
        })
        .catch(err => {
            resolve(null);
        })
    });
}

export const get_transactions = (page = 1, step = 10, account_name = '') => {
    return new Promise((resolve, reject) => {
        axios.post(option.app_host + '/web/get_transactions', {
            page,
            step,
            account_name
        })
        .then((res) => {
            resolve(res.data);
        })
        .catch(err => {
            resolve(null);
        })
    })
}

export const search = ({category, key_word}) => {
    return new Promise((resolve, reject) => {
        axios.post(option.app_host + '/web/search', {
            category,
            key_word
        })
        .then((res) => {
            resolve(res.data);
        })
        .catch(err => {
            resolve(null);
        })
    })
}

export const get_block_detail = (block_num_or_id) => {
    return new Promise((resolve, reject) => {
        axios.post(option.httpEndpoint + '/v1/chain/get_block', {
            block_num_or_id: block_num_or_id
        })
        .then( res => {
            resolve(res.data);
        })
        .catch(err => {
            resolve(null);
        })
    });
}

export const get_transaction_detail = (trx_id) => {
    return new Promise((resolve, reject) => {
        axios.post(option.app_host + '/web/get_transaction', {
            id : trx_id
        })
        .then( res => {
            resolve(res.data);
        })
        .catch(err => {
            resolve(null);
        })
    });
}

export const get_account_transactions = (page = 1, step = 10, account_name) => {
    return new Promise((resolve, reject) => {
        axios.post(option.app_host + '/web/get_account_transactions', {
            page,
            step,
            account_name
        })
        .then((res) => {
            resolve(res.data);
        })
        .catch(err => {
            resolve(null);
        })

    });
}

export const search_eth_eos = (category = '', key_word = '') => {
    return new Promise((resolve, reject) => {
        axios.post(option.app_host + '/web/search_eth_eos', {
            category,
            key_word
        })
        .then((res) => {
            resolve(res.data);
        })
        .catch(err => {
            resolve(null);
        })

    });
}

export const get_account_detail = (account_name) => {
    return new Promise((resolve, reject) => {
        axios.post(option.httpEndpoint + '/v1/chain/get_account', {
            account_name
        })
        .then( res => {
            resolve(res.data);
        });
    });
}

export const get_account_actions = (page = 1, offset = 20, account_name, pre_id = -1, limit = 10**10) => {
    return new Promise((resolve, reject) => {
        axios.post(option.app_host + '/web/get_action',{
            pos: (page - 1) * 20,
            offset: 20,
            account_name,
            limit,
            pre_id
        })
        .then(res => {
            resolve(res.data.data);
        });
    });
}

export const get_action_of_contract = (page = 1, offset = 20, account_name, limit = 10**10) => {
    return new Promise((resolve, reject) => {
        axios.post(option.app_host + '/web/get_action_of_contract',{
            pos: (page - 1) * 20,
            offset: 20,
            account_name,
            limit
        })
        .then(res => {
            resolve(res.data.data);
        });
    });
}

// get_action_of_contract

export const get_account_all_info = (account_name) => {
    return new Promise((resolve, reject) => {
            
        get_account_detail(account_name)
        .then(account_detail => {
            return new Promise((r,j) => {
                r(account_detail);
            });
        })
        .then(account_detail => {
            get_account_info(account_name)
            .then(account_brief => {
                resolve({account_brief, account_detail});
            })
        });

    });
}

// eos_key
export const get_eos_key_accounts = (eos_key, offset = 0) => {
    return new Promise((resolve, reject) => {
        axios.post(option.app_host + '/web/get_eos_key_accounts', {
            eos_key,
            offset
        })
        .then((res) => {
            resolve(res.data);
        })
        .catch(err => {
            resolve(null);
        })
    });
}

// get_user_num
export const get_user_num = () => {
    return new Promise((resolve, reject) => {
        axios.post(option.app_host + '/web/get_user_num')
        .then((res) => {
            resolve(res.data);
        })
        .catch(err => {
            resolve(null);
        })
    });
}

export const get_transaction_total = () => {
    return new Promise((resolve, reject) => {
        axios.post(option.app_host + '/web/get_transaction_total')
        .then((res) => {
            resolve(res.data);
        })
        .catch(err => {
            resolve(null);
        })
    });
}

export const get_info = (trx_id) => {
    return new Promise((resolve, reject) => {
        axios.post(option.httpEndpoint + '/v1/chain/get_info', {
            id : trx_id
        })
        .then(res => {
            resolve(res.data);
        })
        .catch(err => {
            resolve(null);
        });
    });
}

export const get_vote_num = () => {
    return new Promise((resolve, reject) => {
        axios.post(option.app_host + '/web/get_vote_num')
        .then((res) => {
            resolve(res.data);
        })
        .catch(err => {
            resolve(null);
        })
    });
}

export const add_blog = async ({id, author, title, illustration,brief, content, publish_time, is_published = true, password}) => {
    let _form = {id, author, title, illustration, brief, content, publish_time, is_published, password };
    let result = await axios
        .post(option.app_host + '/web/add_blog', _form)
        .then((res) => {
            return res.data;
        })
        .catch(err => {
            return null;
        });
    return result;
}

// get_blog_brief_list
export const get_blog_brief_list = async ({pre = 0, get_max = false}) => {
    let _form = {pre, get_max};
    let result = await axios
        .post(option.app_host + '/web/get_blog_brief_list', _form)
        .then((res) => {
            return res.data;
        })
        .catch(err => {
            return null;
        });
    return result;
}

export const get_blog_item = async ({id = 0}) => {
    let _form = {id};
    let result = await axios
        .post(option.app_host + '/web/get_blog_item', _form)
        .then((res) => {
            return res.data;
        })
        .catch(err => {
            return null;
        });
    return result;
}

export const get_user_reward = async ({user_name = ''}) => {
    let _form = {user_name};
    let result = await axios
        .post(option.app_host + '/web/get_user_reward', _form)
        .then((res) => {
            return res.data;
        })
        .catch(err => {
            return null;
        });
    return result;
}

export const get_my_dapp_reward = async ({user_name = ''}) => {
    let _form = {user_name};
    let result = await axios
        .post(option.app_host + '/web/get_my_dapp_reward', _form)
        .then((res) => {
            return res.data;
        })
        .catch(err => {
            return null;
        });
    return result;
}
// 

export const get_analytics = async () => {
    let result = await axios
        .post(option.app_host + '/web/get_analytics')
        .then((res) => {
            return res.data;
        })
        .catch(err => {
            return null;
        });
    return result;
}

export const get_contract_list = (page = 1, step = 10) => {
    return new Promise((resolve, reject) => {
        axios.post(option.app_host + '/web/get_contract_list', {
            page,
            step
        })
        .then((res) => {
            resolve(res.data);
        })
        .catch(err => {
            resolve(null);
        })
    });
}

export const get_contract_info = (account) => {
    return new Promise((resolve, reject) => {
        axios.post(option.app_host + '/web/get_contract_info', {account})
        .then((res) => {
            resolve(res.data);
        })
        .catch(err => {
            resolve(null);
        })
    });
}

// get_token_list

export const get_token_list = (page = 1, step = 10) => {
    return new Promise((resolve, reject) => {
        axios.post(option.app_host + '/web/get_token_list', {
            page,
            step
        })
        .then((res) => {
            resolve(res.data);
        })
        .catch(err => {
            resolve(null);
        })
    });
}
// 

export const get_token_info_by_symbol = (symbol) => {
    return new Promise((resolve, reject) => {
        axios.post(option.app_host + '/web/get_token_info_by_symbol', {symbol})
        .then((res) => {
            resolve(res.data);
        })
        .catch(err => {
            resolve(null);
        })
    });
}



export const get_action_of_token = (page = 1, offset = 20, symbol, limit = 10**10) => {
    return new Promise((resolve, reject) => {
        axios.post(option.app_host + '/web/get_action_of_token',{
            pos: (page - 1) * 20,
            offset: 20,
            symbol,
            limit
        })
        .then(res => {
            resolve(res.data.data);
        });
    });
}