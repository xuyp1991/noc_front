// import Eos from 'eosforce'
import Eos from 'eosforcejs'
import BigNumber from 'bignumber.js';

import {
    toAsset,
    square_num
} from '../components/utils/utils.js'

import store from '../data/store.js'

const test_node = true

const httpEndpoint = 'https://w3.eosforce.cn'
const network = {
    protocol: 'https',
    blockchain: 'eos',
    host: 'w1.eosforce.cn',
    port: 443,
    chainId: 'bd61ae3a031e8ef2f97ee3b0e62776d6d30d4833c8f7c1645c657b149151004b',
}

const scatter_res = {
    'eos': null,
    'account_name': null,
    'permission': null,
    'has_reject': false,
    'is_running': false
}
/*
    static constexpr uint64_t char_to_symbol( char c ) {
      if( c >= 'a' && c <= 'z' )
         return (c - 'a') + 6;
      if( c >= '1' && c <= '5' )
         return (c - '1') + 1;
      return 0;
   }

   // Each char of the string is encoded into 5-bit chunk and left-shifted
   // to its 5-bit slot starting with the highest slot for the first char.
   // The 13th char, if str is long enough, is encoded into 4-bit chunk
   // and placed in the lowest 4 bits. 64 = 12 * 5 + 4
   static constexpr uint64_t string_to_name( const char* str )
   {
      uint64_t name = 0;
      int i = 0;
      for ( ; str[i] && i < 12; ++i) {
          // NOTE: char_to_symbol() returns char type, and without this explicit
          // expansion to uint64 type, the compilation fails at the point of usage
          // of string_to_name(), where the usage requires constant (compile time) expression.
           name |= (char_to_symbol(str[i]) & 0x1f) << (64 - 5 * (i + 1));
       }

      // The for-loop encoded up to 60 high bits into uint64 'name' variable,
      // if (strlen(str) > 12) then encode str[12] into the low (remaining)
      // 4 bits of 'name'
      if (i == 12)
          name |= char_to_symbol(str[12]) & 0x0F;
      return name;
   }
*/


import ScatterJS from 'scatterjs-core';
import ScatterEOS from 'scatterjs-plugin-eosjs';
ScatterJS.plugins( new ScatterEOS() );
// import Eos from 'eosforce';

const test_new_scatter = async () => {
  
  const network = {
      blockchain:'eos',
      protocol:'https',
      host:'w3.eosforce.cn',
      port:443,
      chainId:'bd61ae3a031e8ef2f97ee3b0e62776d6d30d4833c8f7c1645c657b149151004b'
  }
  let connected = await ScatterJS.scatter.connect('My-App');
  // if(!window.scatter){
  //   alert('end');
  //   return false;
  // }
  // const scatter = window.scatter
  const requiredFields = { accounts:[network] };
  await scatter.getIdentity(requiredFields);
  const account = scatter.identity.accounts.find(x => x.blockchain === 'eos');
  const eosOptions = { expireInSeconds:60 };
  const eos = scatter.eos(network, Eos, eosOptions);
  const transactionOptions = { authorization:[`${account.name}@${account.authority}`] };
  console.log( {eos, account_name: account.name, permission: account.authority, auth: transactionOptions} );
  return {
      is_error: false,
      data: {eos, account_name: account.name, permission: account.authority, auth: transactionOptions}
  };
}


const char_to_symbol = (c) => {
    let code_c = (c + '').charCodeAt(),
        code_a = 'a'.charCodeAt(),
        code_z = 'z'.charCodeAt(),
        code_1 = '1'.charCodeAt(),
        code_5 = '5'.charCodeAt(),
        res = 0;
    if( code_c >= code_a && code_c <= code_z )
         res = (code_c - code_a) + 6;
    if( code_c >= code_1 && code_c <= code_5 )
         res = (code_c - code_1) + 1;
     console.log(c, res, 'char_to_symbol');
      return res
}
var get_max_pos = (ten_num) => {
    let p_n = new Array(64), res = -1;
    for (let i of p_n.keys()) {
        p_n[i] = 2 ** i;
    }
    for (let _index of p_n.keys()) {
        if (ten_num >= p_n[_index] && ten_num < p_n[_index + 1]) {
            res = _index;
            break ;
        }
    }
    return res;
}
var ten_to_bit = (ten_num) => {
    let p_n = new Array(64), res = new Array(64);
    for (let i of p_n.keys()) {
        p_n[i] = 2 ** i;
        res[i] = 0;
    }
    while (ten_num > 0) {
        let pos = get_max_pos(ten_num);
        ten_num -= 2 ** pos;
        res[pos] = 1;
    }
    return res;
}
const move_bite = (bit_num, move_pos) => {
    let new_zero_array = new Array(move_pos);
    for(let _index of new_zero_array.keys()) { new_zero_array[_index] = 0 };
    console.log( new_zero_array );
    bit_num.splice(0, 0, ...new_zero_array);
    return bit_num;
}
const or_bits = (bit_num_from, bit_num_to) => {
    let total = new BigNumber(0);
    for (let _i of bit_num_from.keys()) {
        if (bit_num_from[_i] || bit_num_to[_i]) {
            bit_num_from[_i] = 1;
        }else{
            bit_num_from[_i] = 0;
        }
    }
    return bit_num_from;
}
const bits_to_ten = (bit_num) => {
    let total = new BigNumber(0), keys = bit_num.keys();
    for(let item of keys){
        if ( bit_num[item] ) {
            total = total.plus(square_num(2, item))
        }
    }
    return total;
}
const string_to_name = ( _str ) => {
    let int_num = parseInt(_str);
    if (!int_num || ('' + int_num ).length != (_str + '').length) {
        return _str;
    }
    let name = new Array(64);
    for(let _i of name.keys()) { name[_i] = 0 };
    let i = 0;
    for ( i ; _str[i] && i < 12; ++i) {
       let symbol = ten_to_bit((char_to_symbol(_str[i]) & 0x1f )),
       pos = 64 - 5 * (i + 1) - 1;
       let moved_bites = move_bite(symbol, pos);
       or_bits(name, moved_bites);
   }
   if (i == 12){
      or_bits(name, ten_to_bit((char_to_symbol(_str[12]) & 0x1f )));
   }
   return bits_to_ten(name).toString();
}

export const get_scatter_identity = async (exchange_account = false) => {

    if(scatter_res.is_running){
        return new Promise(async (resolve, reject) => {
            setTimeout(async () => {
              let data = await get_scatter_identity();
              scatter_res.is_running = false;
              resolve(data);
            }, 1000);
        });

        return ;
    }

    scatter_res.is_running = true;

    const network = {
        blockchain:'eos',
        protocol:'https',
        host:'w3.eosforce.cn',
        port:443,
        chainId:'bd61ae3a031e8ef2f97ee3b0e62776d6d30d4833c8f7c1645c657b149151004b'
    }
    let connected = scatter_res.data && scatter_res.data.connected ? scatter_res.data.connected :  await ScatterJS.scatter.connect('My-App');
    if(!connected){
      return {
        is_error: true,
        data: {
          eos: null,
          account_name: null,
          permission: null
        }
      }
    }

    const scatter = ScatterJS.scatter;
    window.scatter = scatter;
    store.dispatch('check_scatter');
    
    if ( exchange_account ) {
       try{
          alert('change_account');
          await scatter.forgetIdentity(); 
       }catch(__){}
    }

    let account = null;
    if( scatter_res.data && scatter_res.data.account ){
      account = scatter_res.data.account;
    }else{
      const requiredFields = { accounts:[network] };
      await scatter.getIdentity(requiredFields);
      account = scatter.identity.accounts.find(x => x.blockchain === 'eos');
    }
    

    const eosOptions = { expireInSeconds:60 };
    const eos = scatter_res.data && scatter_res.data.eos ? scatter_res.data.eos : scatter.eos(network, Eos, eosOptions);
    const transactionOptions = { authorization:[`${account.name}@${account.authority}`] };
    // alert('d3');
    // let token = await eos.contract('eosio');
    // token.transfer(account.name, 'bandon', '0.0001 EOS', 'memo1', transactionOptions).then(trx => {
    //     // That's it!
    //     console.log(`Transaction ID: ${trx.transaction_id}`);
    // }).catch(error => {
    //     console.error(error);
    // });
    console.log( {eos, account_name: account.name, permission: account.authority, auth: transactionOptions} );
    scatter_res.is_running = false;
    scatter_res.data = {eos, account_name: account.name, permission: account.authority, auth: transactionOptions, connected, account};
    return {
        is_error: false,
        data: scatter_res.data
    };
}

const call_scatter = async (call_direct = true) => {

  let data = await get_scatter_identity();
  // alert('data.data.account_name');
  // alert(data.data.account_name);
  // alert(data.data.eos);
  return data;
    let {eos, account_name, permission} = scatter_res;
    if(!call_direct && scatter_res.has_reject){
        return {
            is_error: true,
            msg: scatter_res.error
        }
    }
    let is_error = false;
    let error_msg = '';
    if(!eos){
        let identity_res = await get_scatter_identity();
        is_error = identity_res.is_error;
        error_msg = identity_res.msg;
        alert(account_name);
        if(!is_error){
            eos = identity_res.data.eos;
            account_name = identity_res.data.account_name;
            permission = identity_res.data.permission;
            scatter_res.eos = identity_res.data.eos;
            scatter_res.account_name = identity_res.data.account_name;
            scatter_res.permission = identity_res.data.permission;
        }
    }
    if(is_error){
        return {
            is_error,
            msg: error_msg
        };
    }
    return {eos, account_name, permission}
}

export const vote = async (amount = 2, bpname = '', tokenSymbol = 'EOS') => {
    let call_res = await get_scatter_identity();
    if(call_res.is_error) return call_res;
    let {eos, account_name, permission} = call_res.data;
    let token = await eos.contract('eosio'),
        auth = {
            actor: account_name,
            permission
        }
    return await token.vote(account_name, bpname, toAsset(amount, tokenSymbol), auth)
        .then(data => {
            return {
                is_error: false,
                data
            };
        })
        .catch(err => {
            return {
                is_error: true,
                msg: err
            };
        });
}


export const revote = async (frombp, tobp, restake, tokenSymbol = 'EOS') => {
    let call_res = await get_scatter_identity();
    if(call_res.is_error) return call_res;
    let {eos, account_name, permission} = call_res.data;
    let token = await eos.contract('eosio'),
        auth = {
            actor: account_name,
            permission
        }
    return await token.revote(account_name, frombp, tobp, toAsset(restake, tokenSymbol), auth)
        .then(data => {
            return {
                is_error: false,
                data
            };
        })
        .catch(err => {
            return {
                is_error: true,
                msg: err
            };
        });
}
// revote

export const transfer = async (to = '', amount = 1, memo = '', tokenSymbol = 'EOS') => {
    let call_res = await get_scatter_identity();
    if(call_res.is_error) return call_res;
    let {eos, account_name, permission} = call_res.data;
    let token = await eos.contract(tokenSymbol === 'EOS' ? 'eosio' : 'eosio.token').then(token => { return token }),
        auth = {
            actor: account_name,
            permission
        };
    alert(account_name);
    return await token.transfer(account_name, to, toAsset(amount), memo, auth)
                .then(data => {
                    return {
                        is_error: false,
                        data
                    }
                })
                .catch(error => {
                    return {
                        is_error: true,
                        msg: error
                    }
                })
}

export const transfer_account = async (active_public_key = '', owner_public_key = '', tokenSymbol = 'EOS') => {
    let call_res = await get_scatter_identity();
    if(call_res.is_error) return call_res;
    let {eos, account_name, permission} = call_res.data;
    let token = await eos.contract(tokenSymbol === 'EOS' ? 'eosio' : 'eosio.token').then(token => { return token }),
        auth = {
            actor: account_name,
            permission
        };

    return await token.transaction('eosio', tr => {
      tr.updateauth(account_name, 'active', 'owner', active_public_key, auth);
      tr.updateauth(account_name, 'owner', '', owner_public_key, auth);
    })
    .then(data => {
        return {
            is_error: false,
            data
        }
    })
    .catch(error => {
        return {
            is_error: true,
            msg: error
        }
    })
    return await token.transfer(account_name, to, toAsset(amount), memo, auth)
                .then(data => {
                    return {
                        is_error: false,
                        data
                    }
                })
                .catch(error => {
                    return {
                        is_error: true,
                        msg: error
                    }
                })
}

// let action_res = await token.transaction('eosio', tr => {
//     tr.updateauth(name, 'active', 'owner', active_public_key, auth);
//     tr.updateauth(name, 'owner', '', owner_public_key, auth);
//   })

export const unfreeze = async (bpname = '') => {
    let call_res = await get_scatter_identity();
    if(call_res.is_error) return call_res;
    let {eos, account_name, permission} = call_res.data;
    let auth = {
            actor: account_name,
            permission
        };
    let token = await eos.contract('eosio');
        
    return await token.unfreeze(account_name, bpname, auth)
                .then(data => {
                    return {
                        is_error: false,
                        data
                    };
                })
                .catch(err => {
                    return {
                        is_error: true,
                        msg: err
                    };
                });
};

// test_new_scatter_start

// get_scatter_identity();
// test_new_scatter_end
export const claim = async (bpname) => {
    let call_res = await get_scatter_identity();
    if (call_res.is_error) return call_res;
    let {eos, account_name, permission} = call_res.data;
    // let auth = `${account_name}@${permission}`; 
    let auth = {
            actor: account_name,
            permission
        };

        // let authorization = {"actor":test_account_name,"permission": 'active'};
    let token = await eos.contract('eosio');
    return await token.claim(account_name, bpname, auth)
                .then(data => {
                    return {
                        is_error: false,
                        data
                    };
                })
                .catch(err => {
                    return {
                        is_error: true,
                        msg: err
                    };
                });
};

export const new_account = async (new_name, publicKey) => {
    let call_res = await get_scatter_identity();
    if (call_res.is_error) return call_res;
    let {eos, account_name, permission} = call_res.data;
    let creator = account_name;
    let auth = {
            actor: account_name,
            permission
        };
    let token = await eos.contract('eosio');
    return await token.newaccount(creator, new_name, publicKey, publicKey, auth)
                .then(data => {
                    console.log( data );
                })
                .catch(err => {
                    console.log(err);
                });
}

export const get_my_vote_llist = async () => {
    let call_res = await get_scatter_identity();
    if(call_res.is_error) return call_res;
    let {eos, account_name, permission} = call_res.data;

    if(!account_name){
        return {
            is_error: true,
            msg: ''
        };
    }
    return await Eos({ httpEndpoint })
                .getTableRows({ scope: string_to_name(account_name), code: 'eosio', table: 'votes', json: true, limit: 1000 })
                .then(data => {
                    return {
                        is_error: false,
                        data,
                        account_name
                    };
                })
                .catch(err => {
                    return {
                        is_error: true,
                        msg: err
                    };
                });
}

export const get_available = async () => {
    let call_res = await get_scatter_identity();
    if(call_res.is_error) return call_res;
    let {eos, account_name, permission} = call_res.data;
    if(!account_name){
        return {
            is_error,
            msg: ''
        };
    }
    return await Eos({ httpEndpoint })
                .getTableRows({"scope":"eosio","code":"eosio","table":"accounts","table_key":account_name,"limit":10000,"json":true})
                .then(data => {
                    return {
                        is_error: false,
                        data
                    };
                })
                .catch(err => {
                    return {
                        is_error: true,
                        msg: err
                    };
                });
}

export const get_account = async () => {
  let call_res = await get_scatter_identity(); 
  if(call_res.is_error) return call_res;
  let {eos, account_name, permission} = call_res.data;
  if(!account_name){
      return {
          is_error,
          msg: ''
      };
  }
  return await Eos({ httpEndpoint })
              .getAccount(account_name)
              .then(data => {
                  return {
                      is_error: false,
                      data
                  };
              })
              .catch(err => {
                  return {
                      is_error: true,
                      msg: err
                  };
              });
}

// Eos({ httpEndpoint }).getInfo({});

export const get_top_bps = () => {
    return new Promise(async (resolve, reject) => {
        let {last_irreversible_block_num, head_block_num} = await Eos({ httpEndpoint }).getInfo({});
        let {schedule_version} = await Eos({ httpEndpoint }).getBlock({block_num_or_id: last_irreversible_block_num});
        let top_bps = await Eos({ httpEndpoint }).getTableRows({"scope":"eosio","code":"eosio","table":"schedules","table_key":schedule_version,"json":true,"limit":1000});
        resolve({rows: top_bps.rows[0], schedule_version, head_block_num});
    });
}

export const get_bps = () => {
    return new Promise((resolve, reject) => {
        Eos({ httpEndpoint })
        .getTableRows({ scope: 'eosio', code: 'eosio', table: 'bps', json: true, limit: 1000 })
        .then(async data => {
            resolve({
                is_error: false,
                data: data.rows
            });
        })
        .catch(err => {
            resolve({
                is_error: true,
                msg: err
            });
        })
    });
}


