FROM ubuntu:latest

RUN apt-get update -y --fix-missing
RUN apt-get upgrade -y --fix-missing
RUN apt-get install nodejs -y --fix-missing
RUN apt-get install npm -y --fix-missing
RUN apt-get install wget -y --fix-missing
RUN apt-get install curl -y --fix-missing
RUN apt-get install nginx -y

RUN npm i -g n
RUN n stable
RUN npm i -g babel-node 
RUN npm i -g babel-cli 
RUN npm i -g babel-core 
RUN npm i -g webpack 
RUN npm i -g webpack-cli 
RUN npm i -g webpack-dev-server 
RUN npm i -g pm2

WORKDIR noc_front
ADD . ./
ADD nginx/nginx.conf /etc/nginx/nginx.conf


ENTRYPOINT ["/bin/bash", "./start_in_docker.sh"]

