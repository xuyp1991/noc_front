## 安装服务器需要

    ```bash```
    apt-get install docker.io -y
    apt-get install docker-compose -y

## 应用接口配置
    ```js```
    /**
     编辑 noc_front/src/service/eos_service.js
    **/
    httpEndpoint: 区块节点rpc 端口
    app_host: 后端应用端口, 查看后端配置

    export const option = {
      httpEndpoint: 'http://47.98.238.9:15001',  
      app_host: 'http://0.0.0.0:3002',
    }

## 后端应用启动
    
    ```bash```
    cd [project_path]
    docker-compose up -d

## 本机测试后端接口
    
    ```bash```
    curl http://127.0.0.1:9999/index.html




